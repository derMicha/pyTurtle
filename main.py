import turtle
import Sierpinski

def drawStar():
    turtle.color('red', 'yellow')
    turtle.begin_fill()
    while True:
        turtle.forward(200)
        turtle.left(170)
        if abs(turtle.pos()) < 1:
            break
    turtle.end_fill()

def circles():
    n = 10
    while n <= 100:
        turtle.circle(n)
        n = n + 10

# ‘fastest’ :  0
# ‘fast’    :  10
# ‘normal’  :  6
# ‘slow’    :  3
# ‘slowest’ :  1
turtle.speed(speed=10)

# drawStar()
# circles()
Sierpinski.DrawSierpinskiTriangle(4)

turtle.done()
